import axios from 'axios';

import Vue from 'vue';
import keycloak from '../plugins/keycloak'
import store from '../store'

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
});

apiClient.interceptors.request.use(function (config) {
  return keycloak.updateToken(10).then((refreshed) => {
    if (refreshed) {
      console.log('%capi client %crefreshed token', 'color: #6f42c1', 'color: #6c757d');
    }
    config.headers.Authorization = 'Bearer ' + keycloak.token;
    return config;
  })
}, function (error) {
  // TODO: do something with request error
  return Promise.reject(error)
});

apiClient.interceptors.response.use(function (response) {
  return response;
}, function (error) {
  let errorMessage;

  switch (error.response.status) {
    case 401:
      errorMessage = "You are not authorized to perform this action";
      break;
    case 403:
      errorMessage = "You are not allowed to perform this action";
      break;
    case 404:
      errorMessage = "The requested resource was not found";
      break;
    case 500:
      errorMessage = "Something went wrong on the server, please try again later";
      break;
    default:
      errorMessage = "Something went wrong, please try again later";
      break;
  }  

  const snackbar = {
    show: true,
    text: errorMessage,
    color: "error",
  };

  store.dispatch('setSnackbar', snackbar);
  return Promise.reject(error);
});

Vue.prototype.$apiClient = apiClient;

export default apiClient;