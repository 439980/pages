import apiClient from "../../api/apiClient"

export default {
    state: {
        darkTheme: false,
        navigationDrawerIsOpen: true,
        snackbar: null,
        messages: null,
        users: null,
    },
    getters: {
        darkTheme: (state) => { return state.darkTheme },
        navigationDrawerIsOpen: (state) => { return state.navigationDrawerIsOpen },
        snackbar: (state) => { return state.snackbar },
        messages: (state) => { return state.messages },
        users: (state) => { return state.users }
    },
    actions: {
        enableDarkTheme: ({commit}, isDark)  => {
            commit('SET_DARK_THEME', isDark)
        },
        toggleNavigationDrawer: ({commit, state}) => {
            commit('SET_NAVIGATION_DRAWER', !state.navigationDrawerIsOpen)
        },
        setSnackbar: ({commit}, snackbar) => {
            commit('SET_SNACKBAR', snackbar)
        },
        updateLikes: ({commit}, { uuid, likedUserUuids }) => {
            commit('UPDATE_LIKES', { uuid, likedUserUuids })
        },
        // TODO: implement apiClient
        async fetchMessages({commit}) {
            apiClient.get("/messages").then(response => {
                let messages = response.data
                commit('SET_MESSAGES', messages)
            })
        },
        async fetchUsers({commit}) {
            apiClient.get("/users").then(response => {
                let users = response.data
                commit('SET_USERS', users)
            })
        }
    },
    mutations: {
        SET_DARK_THEME(state, isDark) {
            state.darkTheme = isDark
            localStorage.setItem('theme', isDark ? 'dark' : 'light')
        },
        SET_NAVIGATION_DRAWER(state, isOpen) {
            state.navigationDrawerIsOpen = isOpen
        },
        SET_SNACKBAR(state, snackbar) {
            state.snackbar = snackbar
        },
        SET_MESSAGES(state, messages) {
            state.messages = messages
        },
        UPDATE_LIKES(state, { uuid, likedUserUuids }) {
            let index = state.messages.findIndex(message => message.uuid == uuid)
            if(index > -1) {
                state.messages[index].liked_user_uuids = likedUserUuids
            }
        },
        SET_USERS(state, users) {
            state.users = users
        },
    }
}