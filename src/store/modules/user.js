import keycloak from "../../plugins/keycloak"
import apiClient from "../../api/apiClient"

export default {
    state: {
        uuid: null,
        username: null,
        roles: [],
        firstName: null,
        lastName: null,
        avatar: "man-1",
        bio: "",
        location: "",
        website: "",
        following: null,
        timeline: null,
        isLoadingProfile: false,
    },
    getters: {
        uuid: (state) => { return state.uuid },
        roles: (state) => { return state.roles },
        firstName: (state) => { return state.firstName },
        lastName: (state) => { return state.lastName },
        username: (state) => { return state.username },
        avatar: (state) => { return state.avatar },
        bio: (state) => { return state.bio },
        location: (state) => { return state.location },
        website: (state) => { return state.website },
        profile: (state) => {
            return {
                firstName: state.firstName,
                lastName: state.lastName,
                avatar: state.avatar,
                bio: state.bio,
                location: state.location,
                website: state.website,
            }
        },
        isAdmin: (state) => { 
            return state.roles.includes('admin')
        },
        following: (state) => { return state.following },
        timeline: (state) => { return state.timeline },
        isLoadingProfile: (state) => { return state.isLoadingProfile }

    },
    actions: {
        async initialize({ dispatch, commit }) {
            commit("SET_LOADING_PROFILE", true)
            console.log('%cauth %cinitializing', 'color: #fd7e14', 'color: #6c757d');
            return keycloak.init({ onLoad: 'login-required', promiseType: 'native' })
                .then((authenticated) => {
                    if (authenticated) {
                        console.log('%cauth %cauthenticated', 'color: #fd7e14', 'color: #6c757d');
                        dispatch("loadUserProfile")
                    }
                    else {
                        // TODO: handle error
                    }

                }).catch(e => {
                    console.error('Failed to initialize keycloak: ', e);
                });
        },
        async loadUserProfile({ commit, dispatch }) {
            console.log('%cauth %cloading profile', 'color: #fd7e14', 'color: #6c757d');
            keycloak.loadUserProfile()
                .then(profile => {
                    console.log('%cauth %creceived profile', 'color: #fd7e14', 'color: #6c757d');
                    commit('SET_USER_PROFILE', profile);
                    commit('SET_ROLES', keycloak.realmAccess.roles);
                    return profile
                })
                .catch(e => {
                    console.error('Failed to load profile: ', e);
                })
                .finally(() => {
                    commit('SET_LOADING_PROFILE', false)
                    dispatch("checkFirstLogin");
                })
        },
        logout() {
            let redirectUri = window.location.href;
            keycloak.logout(redirectUri);
        },
        updateProfile: ({ commit }, { firstName, lastName, avatar, bio, location, website }) => {
            commit('UPDATE_PROFILE', { firstName, lastName, avatar, bio, location, website })
        },
        async checkFirstLogin({ state, dispatch }) {
            await apiClient.get('/users/' + state.uuid)
            // if no user exists with this uuid, create one
            .then(response => {
                let profile = response.data;
                dispatch('updateProfile', {
                    firstName: profile.firstName,
                    lastName: profile.lastName,
                    avatar: profile.avatar,
                    bio: profile.bio,
                    location: profile.location,
                    website: profile.website
                });
            })
            .catch(err => {
                if(err.response.status === 404) {
                    apiClient.post('/users', {
                        uuid: state.uuid,
                        username: state.username,
                        firstName: state.firstName,
                        lastName: state.lastName,
                        avatar: state.avatar,
                        bio: state.bio,
                        location: state.location,
                        website: state.website
                    })
                    .catch(err => {
                        console.error(err)
                    })
                }
            })
            .finally(() => {
                dispatch("fetchUsers");
                dispatch("fetchFollowing");
                dispatch("fetchTimeline");
                dispatch("fetchMessages");
            })
        },
        async fetchFollowing({ state, commit }) {
            apiClient.get(`/users/following/${state.uuid}`).then(response => {
                commit('SET_FOLLOWING', response.data)
            })
        },
        async followUser({ state, commit }, uuid) {
            let followRequestBody = {
                follower_uuid: state.uuid,
                followee_uuid: uuid,
                date: new Date().toISOString()
            }
            console.log(uuid)
            apiClient.post(`/users/follow`, followRequestBody).then(response => {
                commit('ADD_TO_FOLLOWING', response.data)
            })
        },
        async unfollowUser({ state, commit }, uuid) {
            let unfollowRequestBody = {
                follower_uuid: state.uuid,
                followee_uuid: uuid
            }
            
            apiClient.post(`/users/unfollow`, unfollowRequestBody).then(response => {
                commit('REMOVE_FROM_FOLLOWING', response.data)
            })
        },
        async fetchTimeline({ state, commit }) {
            apiClient.get(`/timelines/${state.uuid}`).then(response => {
                let timeline = response.data.messages
                commit('SET_TIMELINE', timeline)
            })
        }
    },
    mutations: {
        UPDATE_PROFILE(state, { firstName, lastName, avatar, bio, location, website }) {
            state.firstName = firstName
            state.lastName = lastName
            state.avatar = avatar
            state.bio = bio
            state.location = location
            state.website = website
        },
        SET_USER_PROFILE(state, profile) {
            state.uuid = profile.id,
            state.username = profile.username,
            state.firstName = profile.firstName,
            state.lastName = profile.lastName
        },
        SET_ROLES(state, roles) {
            state.roles = roles
        },
        SET_LOADING_PROFILE(state, loading) {
            state.isLoadingProfile = loading
        },
        SET_FOLLOWING(state, following) {
            state.following = following
        },
        ADD_TO_FOLLOWING(state, followee) {   
            state.following.push(followee)
        },
        REMOVE_FROM_FOLLOWING(state, followee) {
            state.following = state.following.filter(f => f.uuid !== followee.uuid)
        },
        SET_TIMELINE(state, timeline) {
            state.timeline = timeline
        }  
    }
}