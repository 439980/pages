import Vue from 'vue';
import Vuetify from 'vuetify/lib/framework';

Vue.use(Vuetify);

export default new Vuetify({
    theme: {
        themes: {
            light:  {
                primary: '#D5A021',
                secondary: '#8A7F72',
                background: '#E5E5E5',
                page: '#FFFFFF'
            },
            dark:  {
                primary: '#D5A021',
                secondary: '#8A7F72',
                background: '#202124',
                page: '#000000'
            },
        },
    },
});
