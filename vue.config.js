module.exports = {
  pages: {
    index: {
      entry: 'src/main.js',
      title: 'Qwetter',
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'en',
      fallbackLocale: 'en',
      localeDir: 'locales',
      enableInSFC: false,
      enableBridge: false
    }
  },

  transpileDependencies: [
    'vuetify'
  ],

  publicPath: './'
  // NOTE: code below disables docker and production builds, but makes it work with GitLab pages
  // publicPath: process.env.NODE_ENV === 'production'
  //   ? '/' + process.env.CI_PROJECT_NAME + '/'
  //   : '/'
}
